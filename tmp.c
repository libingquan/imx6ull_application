#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdlib.h>
#include <execinfo.h>

static int fd;


/* Obtain a backtrace and print it to stdout. */
void print_trace (void)
{
	void *array[10];
	char **strings;
	int size, i;

	size = backtrace (array, 10);
	strings = backtrace_symbols (array, size);
	if (strings != NULL)
	{

		printf ("Obtained %d stack frames.\n", size);
		for (i = 0; i < size; i++)
		  printf ("%s\n", strings[i]);
	}

	free (strings);
}

void dummy_func(void){
    printf("\n ------------------------------------- \n");
    print_trace();
}


int main(int argc, char *argv[]){
    if (0 > (fd = open("/dev/input/event2", O_RDONLY))) {
        printf("cannot open /dev/input/event2\n");
        return -1;
    }

    struct input_event in_ev = {0};

    while(1){
        read(fd, &in_ev, sizeof(struct input_event));
        if (EV_KEY == in_ev.type) { //按键事件
            switch (in_ev.value) {
                case 0:
                    printf("code<%d>: 松开\n", in_ev.code);
                    break;
                case 1:
                    printf("code<%d>: 按下\n", in_ev.code);
                    break;
                case 2:
                    printf("code<%d>: 长按\n", in_ev.code);
                    break;
            }
            dummy_func();
        }
    }
    close(fd);
    return 0;
}


