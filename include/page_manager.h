#ifndef _PAGE_MANAGER_H
#define _PAGE_MANAGER_H

typedef struct PageAction {
	char *name;
	void (*Run)(void *pParams);
	void (*Exit)(void *pParams);
	struct PageAction *ptParent;
	struct PageAction *ptNext;
}T_PageAction, *PT_PageAction;

void PageRegister(PT_PageAction ptPageAction);
void PageSystemRegister(void);
PT_PageAction Page(char *name);

PT_PageAction GetCurPage(void);
void SetParent(PT_PageAction ptPAtarget, PT_PageAction ptPAparent);
void RunPage(char *strName);


#endif




