#ifndef _DISP_MANAGER_H
#define _DISP_MANAGER_H

#include <common.h>
#include <font_manager.h>

#include <pic_operation.h>
#include <video_manager.h>

typedef struct DispBuff {
	int iXres;			/* X分辨率 */
	int iYres;			/* X分辨率 */
	int iBpp;			/* 一个像素用多少位来表示 */
	int iLineWidth;		/* 一行数据占据多少字节 */
	unsigned char *buff;			/* 显存地址 */
}T_DispBuff, *PT_DispBuff;


/* 显示区域,含该区域的左上角/右下角座标
 * 如果是图标,还含有图标的文件名
 */
typedef struct Layout {
	int iTopLeftX;
	int iTopLeftY;
	int iBotRightX;
	int iBotRightY;
	char *strIconName;
}T_Layout, *PT_Layout;


/* VideoMem的状态:
 * 空闲/用于预先准备显示内容/用于当前线程
 */
typedef enum {
	VMS_FREE = 0,
	VMS_USED_FOR_PREPARE,
	VMS_USED_FOR_CUR,	
}E_VideoMemState;

/* VideoMem中内存里图片的状态:
 * 空白/正在生成/已经生成
 */
typedef enum {
	PS_BLANK = 0,
	PS_GENERATING,
	PS_GENERATED,	
}E_PicState;


typedef struct VideoMem {
	int iID;                        /* ID值,用于标识不同的页面 */
	int bDevFrameBuffer;            /* 1: 这个VideoMem是显示设备的显存; 0: 只是一个普通缓存 */
	E_VideoMemState eVideoMemState; /* 这个VideoMem的状态 */
	E_PicState ePicState;           /* VideoMem中内存里图片的状态 */
	T_PixelDatas tPixelDatas;       /* 内存: 用来存储图像 */
	struct VideoMem *ptNext;        /* 链表 */
}T_VideoMem, *PT_VideoMem;


typedef struct DispOpr {
	char *name;
	int (*DeviceInit)(void);										/* 设备初始化函数 */
	int (*DeviceExit)(void);										/* 设备退出函数 */
	int (*GetBuffer)(PT_DispBuff ptDispBuff);						/* 获取显存地址以及大小数据 */
	int (*ShowPage)(PT_PixelDatas ptPixelDatas);					/* 显示一页,数据源自ptVideoMem */
	int (*CleanScreen)(unsigned int dwBackColor);                   /* 清屏为某颜色 */
	int (*FlushRegion)(PRegion ptRegion, PT_DispBuff ptDispBuff);	
	struct DispOpr *ptNext;
}T_DispOpr, *PT_DispOpr;

// 注册"显示模块", 把所能支持的显示设备的操作函数放入链表
void RegisterDisplay(PT_DispOpr ptDispOpr);

// 注册显示设备
void DisplaySystemRegister(void);

// 根据名字选取默认的"显示模块"
int SelectDefaultDisplay(char *name);

// 初始化默认的"显示模块"
int InitDefaultDisplay(void);

// 获得所使用的显示设备的分辨率和BPP
int GetDispResolution(int *piXres, int *piYres, int *piBpp);

int GetVideoBufForDisplay(PT_VideoBuf ptFrameBuf);

void FlushPixelDatasToDev(PT_PixelDatas ptPixelDatas);

PT_DispOpr GetDefaultDispDev(void);

// 为加快显示速度,事先在缓存（VideoMem）中构造好显示的页面的数据, 显示时再把VideoMem中的数据复制到设备的显存上
int AllocVideoMem(int iNum);

// 获得显示设备的显存, 在该显存上操作就可以直接在LCD上显示出来
PT_VideoMem GetDevVideoMem(void);

// 获得一块可操作的VideoMem(它用于存储要显示的数据), 用完后用PutVideoMem来释放
PT_VideoMem GetVideoMem(int iID, int bCur);

// 释放 VideoMem
void PutVideoMem(PT_VideoMem ptVideoMem);

// 把VideoMem中内存全部清为某种颜色
void ClearVideoMem(PT_VideoMem ptVideoMem, unsigned int dwColor);

// 把VideoMem中内存的指定区域全部清为某种颜色
void ClearVideoMemRegion(PT_VideoMem ptVideoMem, PT_Layout ptLayout, unsigned int dwColor);


int PutPixel(int x, int y, unsigned int dwColor);
int FlushDisplayRegion(PRegion ptRegion, PT_DispBuff ptDispBuff);
PT_DispBuff GetDisplayBuffer(void);
void DrawFontBitMap(PFontBitMap ptFontBitMap, unsigned int dwColor);
void DrawRegion(PRegion ptRegion, unsigned int dwColor);
void DrawTextInRegionCentral(char *name, PRegion ptRegion, unsigned int dwColor);


#endif




