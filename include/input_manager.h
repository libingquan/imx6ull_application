#ifndef _INPUT_MANAGER_H
#define _INPUT_MANAGER_H

#include <sys/time.h>

#ifndef NULL
#define NULL (void *)0
#endif

#define INPUT_TYPE_TOUCH	1
#define INPUT_TYPE_NET		2
#define INPUT_TYPE_BUTTON	3

typedef struct InputEvent {
	struct timeval	tTime;
	int iType;
	int iX;
	int iY;
	int iPressure;
	char str[1024];
	int iValue;
	int iCode;
}InputEvent, *PInputEvent;


typedef struct InputDevice {
	char *name;
	int (*GetInputEvent)(PInputEvent ptInputEvent);
	int (*DeviceInit)(void);
	int (*DeviceExit)(void);
	struct InputDevice *ptNext;
}InputDevice, *PInputDevice;


void RegisterInputDevice(PInputDevice ptInputDev);
void InputSystemRegister(void);
void IntpuDeviceInit(void);
int GetInputEvent(PInputEvent ptInputEvent);


typedef struct EventHandler {
    char *name;
    void *(*HandleFunc)(void*);
    struct EventHandler *ptNext;
}T_EventHandler, *PT_EventHandler;


void EventHandlerRegister(PT_EventHandler ptEventHandler);
void RemoveHandler(PT_EventHandler handlers);
void EventHandlerInit(void);


#endif




