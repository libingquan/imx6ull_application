
#include <config.h>
#include <common.h>
#include <stdio.h>
#include <ui.h>
#include <input_manager.h>
#include <page_manager.h>
#include <disp_manager.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#include <time.h>

#define X_GAP 5
#define Y_GAP 5

static T_PageAction g_tMainPage;

static Button g_tButtons[ITEMCFG_MAX_NUM];
static int g_tButtonCnt;

static unsigned char main_page_status = 0;
static pthread_mutex_t g_tMutex  = PTHREAD_MUTEX_INITIALIZER;
static char strNextPage[100];

static int MainPageOnPressed(struct Button *ptButton, PT_DispBuff ptDispBuff, PInputEvent ptInputEvent)
{
	unsigned int dwColor = BUTTON_DEFAULT_COLOR;
	char name[100];
	char status[100];
	char *strButton;

	strButton = ptButton->name;
	
	/* 1. 对于触摸屏事件 */
	if (ptInputEvent->iType == INPUT_TYPE_TOUCH)
	{
		/* 1.1 分辨能否被点击 */
		if (GetItemCfgByName(ptButton->name)->bCanBeTouched == 0)
			return -1;

		// 仅针对按下生效
		if (ptInputEvent->iPressure <= 0)
			return -1;

		/* 1.2 修改颜色 */
		ptButton->status = !ptButton->status;
		if (ptButton->status)
			dwColor = BUTTON_PRESSED_COLOR;
	}
	else if (ptInputEvent->iType == INPUT_TYPE_NET)
	{
		/* 2. 对于网络事件 */
		
		/* 根据传入的字符串修改颜色 : wifi ok, wifi err, burn 70 */
		sscanf(ptInputEvent->str, "%s %s", name, status);
		if (strcmp(status, "ok") == 0)
			dwColor = BUTTON_PRESSED_COLOR;
		else if (strcmp(status, "err") == 0)
			dwColor = BUTTON_DEFAULT_COLOR;
		else if (status[0] >= '0' && status[0] <= '9')
		{			
			dwColor = BUTTON_PERCENT_COLOR;
			strButton = status;			
		}
		else
			return -1;
	}
	else if (ptInputEvent->iType == INPUT_TYPE_BUTTON){
		/* todo 对于按键事件 */
        printf("按键事件响应: code<%d>: 按下\n", ptInputEvent->iValue);
	}
	else
	{
		return -1;
	}

	/* 绘制底色 */
	DrawRegion(&ptButton->tRegion, dwColor);

	/* 居中写文字 */
	DrawTextInRegionCentral(strButton, &ptButton->tRegion, BUTTON_TEXT_COLOR);

	/* flush to lcd/web */
	FlushDisplayRegion(&ptButton->tRegion, ptDispBuff);

	PItemCfg ptItemCfg;
	ptItemCfg = GetItemCfgByName(ptButton->name);
	if(ptItemCfg->command[0] != '\0')
		system(ptItemCfg->command);

	/* 具体按钮逻辑 */
	if (strcmp(ptButton->name, "camera") == 0){
		/* 点击 camera ：切换到显示摄像头界面 */
		strcpy(strNextPage, "video");
		pthread_mutex_lock(&g_tMutex);
		main_page_status = 0;
		pthread_mutex_unlock(&g_tMutex);
		// RunPage("video");
		// Page("main")->Exit(NULL);
		// Page("video")->Run(NULL);
	}

	return 0;
}

static int GetFontSizeForAllButton(void)
{
	int i;
	int max_len = -1;
	int max_index = 0;
	int len;
	RegionCartesian tRegionCar;
	float k, kx, ky;
	
	/* 1. 找出name最长的Button */
	for (i = 0; i < g_tButtonCnt; i++)
	{
		len = strlen(g_tButtons[i].name);
		if (len > max_len)
		{
			max_len = len;
			max_index = i;
		}
	}

	/* 2. 以font_size =100, 算出它的外框 */
	SetFontSize(100);
	GetStringRegionCar(g_tButtons[max_index].name, &tRegionCar);

	/* 3. 把文字的外框缩放为Button的外框 */
	kx = (float)g_tButtons[max_index].tRegion.iWidth / tRegionCar.iWidth;
	ky = (float)g_tButtons[max_index].tRegion.iHeigh / tRegionCar.iHeigh;
	//printf("button width / str width   = %d/%d = %f\n", g_tButtons[max_index].tRegion.iWidth, tRegionCar.iWidth, kx);
	//printf("button height / str height = %d/%d = %f\n", g_tButtons[max_index].tRegion.iHeigh, tRegionCar.iHeigh, ky);
	if (kx < ky)
		k = kx;
	else
		k = ky;

	//printf("font size = %d\n", (int)(k*100));
	/* 4. 反算出font size, 只取0.80, 避免文字过于接近边界 */
	return k * 100 * 0.8;
}

static void GenerateButtons(void)
{
	int width, height;
	int n_per_line;
	int row, rows;
	int col;
	int n;
	PT_DispBuff ptDispBuff;
	int xres, yres;
	int start_x, start_y;
	int pre_start_x, pre_start_y;
	PButton pButton;
	int i = 0;
	int iFontSize;
	
	/* 算出单个按钮的width/height */
	g_tButtonCnt = n = GetItemCfgCount();
	
	ptDispBuff = GetDisplayBuffer();
	xres = ptDispBuff->iXres;
	yres = ptDispBuff->iYres;
	width = sqrt(1.0/0.618 *xres * yres / n);
	n_per_line = xres / width + 1;
	width  = xres / n_per_line;
	height = 0.618 * width;	

	/* 居中显示:  计算每个按钮的region  */
	start_x = (xres - width * n_per_line) / 2;
	rows    = n / n_per_line;
	if (rows * n_per_line < n)
		rows++;
	
	start_y = (yres - rows*height)/2;

	/* 计算每个按钮的region */
	for (row = 0; (row < rows) && (i < n); row++)
	{
		pre_start_y = start_y + row * height;
		pre_start_x = start_x - width;
		for (col = 0; (col < n_per_line) && (i < n); col++)
		{
			pButton = &g_tButtons[i];
			pButton->tRegion.iLeftUpX = pre_start_x + width;
			pButton->tRegion.iLeftUpY = pre_start_y;
			pButton->tRegion.iWidth   = width - X_GAP;
			pButton->tRegion.iHeigh   = height - Y_GAP;
			pre_start_x = pButton->tRegion.iLeftUpX;

			/* InitButton */
			InitButton(pButton, GetItemCfgByIndex(i)->name, NULL, NULL, MainPageOnPressed);
			i++;
		}
	}

	iFontSize = GetFontSizeForAllButton();
	//SetFontSize(iFontSize);

	/* 清屏 */
	PT_DispOpr ptDispOpr;
	ptDispOpr = GetDefaultDispDev();
	ptDispOpr->CleanScreen(0);

	/* OnDraw */
	for (i = 0; i < n; i++)
	{
		g_tButtons[i].iFontSize = iFontSize;
		g_tButtons[i].OnDraw(&g_tButtons[i], ptDispBuff);
	}
}

static int isTouchPointInRegion(int iX, int iY, PRegion ptRegion)
{
	if (iX < ptRegion->iLeftUpX || iX >= ptRegion->iLeftUpX + ptRegion->iWidth)
		return 0;

	if (iY < ptRegion->iLeftUpY || iY >= ptRegion->iLeftUpY + ptRegion->iHeigh)
		return 0;

	return 1;
}


static PButton GetButtonByName(char *name)
{
	int i;
	
	for (i = 0; i < g_tButtonCnt; i++)
	{
		if (strcmp(name, g_tButtons[i].name) == 0)
			return &g_tButtons[i];
	}

	return NULL;
}


static PButton GetButtonByInputEvent(PInputEvent ptInputEvent)
{
	int i;
	char name[100];
	
	if (ptInputEvent->iType == INPUT_TYPE_TOUCH)
	{
		for (i = 0; i < g_tButtonCnt; i++)
		{
			if (isTouchPointInRegion(ptInputEvent->iX, ptInputEvent->iY, &g_tButtons[i].tRegion))
				return &g_tButtons[i];
		}
	}
	else if (ptInputEvent->iType == INPUT_TYPE_NET)
	{
		sscanf(ptInputEvent->str, "%s", name);
		return GetButtonByName(name);
	}
	else if (ptInputEvent->iType == INPUT_TYPE_BUTTON){
		// todo
		switch (ptInputEvent->iCode) {
            case 0:
                printf("GetButtonByInputEvent: button-key code<%d>: 松开\n", ptInputEvent->iValue);
                break;
            case 1:
                printf("GetButtonByInputEvent: button-key code<%d>: 按下\n", ptInputEvent->iValue);
                break;
            case 2:
                printf("GetButtonByInputEvent: button-key code<%d>: 长按\n", ptInputEvent->iValue);
                break;
        }

		if (ptInputEvent->iValue == 3 && ptInputEvent->iCode == 1){
			printf("\n\n 按下结束按钮 \n");
			GetDefaultDispDev()->CleanScreen(0);
			exit(0);
		}
	}
	else
	{
		return NULL;
	}
	return NULL;
}


static void *EventHandler(void *data){
	/* 根据输入事件找到按钮 */
	PButton ptButton;
	PT_DispBuff ptDispBuff = GetDisplayBuffer();
	PInputEvent ptInputEvent = (PInputEvent) data;

	ptButton = GetButtonByInputEvent(ptInputEvent);
	if (!ptButton)
		return NULL;

	/* 调用按钮的OnPressed函数 */
	ptButton->OnPressed(ptButton, ptDispBuff, ptInputEvent);
	return NULL;
}

static void MainPageRun(void *pParams)
{
	int error;
	
	/* 读取配置文件 */
	if (GetItemCfgCount() <= 0){
		error = ParseConfigFile();
		if (error)
			return ;
	}

	/* 根据配置文件生成按钮、界面 */
	GenerateButtons();

	/* 注册响应函数 */
	T_EventHandler handler;
	handler.name = g_tMainPage.name;
	handler.HandleFunc = EventHandler;
	EventHandlerRegister(&handler);

	struct timespec req;
	req.tv_sec = 0;
	req.tv_nsec = 1000000;

	pthread_mutex_lock(&g_tMutex);
	main_page_status = 1;
	pthread_mutex_unlock(&g_tMutex);
	while (main_page_status)
	{
		nanosleep(&req, NULL);	// 休眠 1 ms
	}

	// 退出循环意味着切换界面
	RunPage(strNextPage);
}

static void MainPageExit(void *pParams){
	/* 退出页面逻辑 */
	main_page_status = 0;

	/* 清屏 */
	PT_DispOpr ptDispOpr;
	ptDispOpr = GetDefaultDispDev();
	ptDispOpr->CleanScreen(0);
}

static T_PageAction g_tMainPage = {
	.name = "main",
	.Run  = MainPageRun,
	.Exit = MainPageExit,
};

void MainPageRegister(void)
{
	PageRegister(&g_tMainPage);
}


