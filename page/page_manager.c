
#include <common.h>
#include <page_manager.h>
#include <string.h>

static PT_PageAction g_ptPages = NULL;
static PT_PageAction g_ptCurPage = NULL;

void PageRegister(PT_PageAction ptPageAction)
{
	ptPageAction->ptNext = g_ptPages;
	g_ptPages = ptPageAction;
}

PT_PageAction Page(char *name)
{
	PT_PageAction ptTmp = g_ptPages;

	while (ptTmp)
	{
		if (strcmp(name, ptTmp->name) == 0)
			return ptTmp;
		ptTmp = ptTmp->ptNext;
	}

	return NULL;
}


void SetParent(PT_PageAction ptPAtarget, PT_PageAction ptPAparent){
	if (ptPAtarget && ptPAparent){
		ptPAtarget->ptParent = ptPAparent;
	}
}


void RunPage(char *strName){
	if (g_ptCurPage)
		g_ptCurPage->Exit(NULL);
	
	PT_PageAction ptPage;

	// 不指定，默认返回上一级界面
	if (!strName){
		ptPage = g_ptCurPage->ptParent;
	}
	else
		ptPage = Page(strName);
	
	if (ptPage){
		g_ptCurPage = ptPage;
		ptPage->Run(NULL);
	}
	else
		g_ptCurPage = NULL;
}


PT_PageAction GetCurPage(void){
	return g_ptCurPage;
}


void PageSystemRegister(void)
{
	extern void MainPageRegister(void);
	MainPageRegister();

	extern void VideoPageRegister(void);
	VideoPageRegister();

	SetParent(Page("video"), Page("main"));
	
}

