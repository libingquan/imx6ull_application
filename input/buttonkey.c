#include <input_manager.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/input.h>

static int fd;

static int ButtonkeyGetInputEvent(PInputEvent ptInputEvent){
    struct input_event in_ev = {0};
    read(fd, &in_ev, sizeof(struct input_event));
    if (EV_KEY == in_ev.type) { //按键事件
        // switch (in_ev.value) {
        //     case 0:
        //         printf("code<%d>: 松开\n", in_ev.code);
        //         break;
        //     case 1:
        //         printf("code<%d>: 按下\n", in_ev.code);
        //         break;
        //     case 2:
        //         printf("code<%d>: 长按\n", in_ev.code);
        //         break;
        // }

        ptInputEvent->iType = INPUT_TYPE_BUTTON;
        ptInputEvent->iValue = in_ev.code;
        ptInputEvent->iCode = in_ev.value;

        return 0;
    }
    return -1;
}

static int ButtonkeyDeviceInit(void){
    if (0 > (fd = open("/dev/input/event2", O_RDONLY))) {
        printf("cannot open /dev/input/event2\n");
        return -1;
    }

    return 0;
}

static int ButtonkeyDeviceExit(void){
    close(fd);
    return 0;
}


static InputDevice g_tButtonkeyDev ={
	.name = "buttonkey",
	.GetInputEvent  = ButtonkeyGetInputEvent,
	.DeviceInit     = ButtonkeyDeviceInit,
	.DeviceExit     = ButtonkeyDeviceExit,
};

void ButtonkeyRegister(void)
{
	RegisterInputDevice(&g_tButtonkeyDev);
}
