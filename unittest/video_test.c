#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <linux/fb.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdlib.h>

#include <disp_manager.h>
#include <font_manager.h>
#include <input_manager.h>
#include <page_manager.h>
#include <video_manager.h>
#include <convert_manager.h>
#include <render.h>


int main(int argc, char **argv)
{
	int error;

	if (argc != 3)
	{
		printf("Usage: %s <font_file> </dev/video0,1,...>\n", argv[0]);
		return -1;
	}
	
	/* 初始化显示系统 */		
	DisplaySystemRegister();  /* 以前是: DisplayInit(); */

	SelectDefaultDisplay("fb");

	InitDefaultDisplay();

	/* 初始化输入系统 */		
	InputSystemRegister(); /* 以前是: InputInit(); */
	IntpuDeviceInit();


	/* 初始化文字系统 */		
	FontSystemRegister(); /* 以前是: FontsRegister(); */
	
	error = SelectAndInitFont("freetype", argv[1]);
	if (error)
	{
		printf("SelectAndInitFont err\n");
		return -1;
	}

    /* 初始化摄像头模块 */
    VideoInit();
	SetVideoDeviceName(argv[2]);
    VideoConvertInit();

	/* 初始化页面系统 */		
	PageSystemRegister(); /* 以前是: PagesRegister(); */
	EventHandlerInit();

	/* 运行业务系统的主页面 */
	RunPage("main");
	
	return 0;	
}




